<?php
/**
 * Created by PhpStorm.
 * User: guillermo
 * Date: 19/09/19
 * Time: 03:56 PM
 */

namespace Madkting\Marketplace\Model\Data;


use Madkting\Marketplace\Api\Data\MagentoInfoInterface;
use Magento\Framework\Api\AbstractSimpleObject;

/**
 * Class MagentoInfo
 * @package Madkting\Marketplace\Model\Data
 */
class MagentoInfo extends AbstractSimpleObject implements MagentoInfoInterface
{
    /**
     * Magento information keys
     */
    const KEY_VERSION = 'version';
    const KEY_EDITION = 'edition';
    const KEY_NAME = 'name';

    /**
     * Get Magento version.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->_get(self::KEY_VERSION);
    }

    /**
     * Set Magento version.
     *
     * @param $version
     * @return MagentoInfo
     */
    public function setVersion($version)
    {
        return $this->setData(self::KEY_VERSION, $version);
    }

    /**
     * Get Magento's edition.
     *
     * @return string
     */
    public function getEdition()
    {
        return $this->_get(self::KEY_EDITION);
    }

    /**
     * Set Magento's edition.
     *
     * @param string $edition
     * @return $this
     */
    public function setEdition($edition)
    {
        return $this->setData(self::KEY_EDITION, $edition);
    }

    /**
     * Get Magento's name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->_get(self::KEY_NAME);
    }

    /**
     * Set Magento's name.
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData(self::KEY_NAME, $name);
    }
}
