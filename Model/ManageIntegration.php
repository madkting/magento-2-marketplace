<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Marketplace
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento-2-marketplace
 * @copyright Copyright (c) 2018 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Marketplace\Model;

use Magento\Integration\Model\AuthorizationServiceFactory;
use Magento\Integration\Model\Integration;
use Magento\Integration\Model\IntegrationFactory;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Integration\Model\OauthServiceFactory;
use Magento\Integration\Model\ResourceModel\Integration as IntegrationResource;

/**
 * Class ManageIntegration
 * @package Madkting\Marketplace\Model
 */
class ManageIntegration
{
    /**
     * Integration general information
     */
    const INTEGRATION_NAME = 'Yuju Marketplace';
    const INTEGRATION_EMAIL = '';
    const INTEGRATION_CALLBACK_URL = '';

    /**
     * @var IntegrationFactory
     */
    private $integrationFactory;

    /**
     * @var IntegrationResource
     */
    private $integrationResource;

    /**
     * @var OauthServiceFactory
     */
    private $oauthServiceFactory;

    /**
     * @var AuthorizationServiceFactory
     */
    private $authorizationServiceFactory;

    /**
     * @var TokenFactory
     */
    private $tokenFactory;

    /**
     * ManageIntegration constructor
     *
     * @param IntegrationFactory $integrationFactory
     * @param IntegrationResource $integrationResource
     * @param OauthServiceFactory $oauthServiceFactory
     * @param AuthorizationServiceFactory $authorizationServiceFactory
     * @param TokenFactory $tokenFactory
     */
    public function __construct(
        IntegrationFactory $integrationFactory,
        IntegrationResource $integrationResource,
        OauthServiceFactory $oauthServiceFactory,
        AuthorizationServiceFactory $authorizationServiceFactory,
        TokenFactory $tokenFactory
    ) {
        $this->integrationFactory = $integrationFactory;
        $this->integrationResource = $integrationResource;
        $this->oauthServiceFactory = $oauthServiceFactory;
        $this->authorizationServiceFactory = $authorizationServiceFactory;
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * Create Madkting Marketplace Integration
     *
     * @return void
     */
    public function createIntegration()
    {
        /**
         * Check if integration already exists
         *
         * @var Integration $integrationModel
         */
        $integrationModel = $this->integrationFactory->create();
        $this->integrationResource->load($integrationModel, self::INTEGRATION_NAME, Integration::NAME);
        if (empty($integrationModel->getId())) {

            /* Create Madkting Marketplace integration */
            $integrationModel->setName(self::INTEGRATION_NAME)
                ->setEmail(self::INTEGRATION_EMAIL);
            $this->integrationResource->save($integrationModel);

            /* Create consumer */
            if (!empty($integrationModel->getId())) {
                $consumerName = 'Integration' . $integrationModel->getId();

                /** @var \Magento\Integration\Model\Oauth\Consumer $consumerModel */
                $consumerModel = $this->oauthServiceFactory->create()->createConsumer(['name' => $consumerName]);

                /* Add consumer ID to integration */
                if (!empty($consumerModel->getId())) {
                    $integrationModel->setConsumerId($consumerModel->getId());
                    $this->integrationResource->save($integrationModel);

                    /* Grant permissions */
                    $this->authorizationServiceFactory->create()->grantAllPermissions($integrationModel->getId());

                    /**
                     * Create access token
                     *
                     * @var \Magento\Integration\Model\Oauth\Token $accessToken
                     */
                    $accessToken = $this->tokenFactory->create();
                    $accessToken->createVerifierToken($consumerModel->getId());
                    $accessToken->createRequestToken($accessToken->getId(), self::INTEGRATION_CALLBACK_URL);
                    $accessToken->convertToAccess();

                    /* Activate integration */
                    $integrationModel->setStatus(Integration::STATUS_ACTIVE);
                    $this->integrationResource->save($integrationModel);
                }
            }
        }
    }
}
