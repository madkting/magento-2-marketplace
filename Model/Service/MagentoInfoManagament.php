<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Marketplace
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento-2-marketplace
 * @copyright Copyright (c) 2018 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Marketplace\Model\Service;

use Madkting\Marketplace\Api\Data\MagentoInfoInterface;
use Madkting\Marketplace\Api\MagentoInfoManagamentInterface;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Class MagentoInfoManagament
 * @package Madkting\Marketplace\Model\Service
 * @api
 */
class MagentoInfoManagament implements MagentoInfoManagamentInterface
{
    /**
     * @var MagentoInfoInterface
     */
    protected $magentoInfo;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * MagentoInfo constructor.
     * @param MagentoInfoInterface $magentoInfo
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        MagentoInfoInterface $magentoInfo,
        ProductMetadataInterface $productMetadata
    ) {
        $this->magentoInfo = $magentoInfo;
        $this->productMetadata = $productMetadata;
    }

    /**
     * Retrieve Magento information.
     *
     * @return MagentoInfoInterface
     */
    public function getGeneralInfo()
    {
        $version = $this->productMetadata->getVersion();
        $this->magentoInfo->setVersion($version);
        $edition = $this->productMetadata->getEdition();
        $this->magentoInfo->setEdition($edition);
        $name = $this->productMetadata->getName();
        $this->magentoInfo->setName($name);

        return $this->magentoInfo;
    }
}
