<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Marketplace
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento-2-marketplace
 * @copyright Copyright (c) 2018 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Marketplace\Api\Data;

/**
 * Interface MagentoInfoInterface
 * @package Madkting\Marketplace\Api\Data
 * @api
 */
interface MagentoInfoInterface
{
    /**
     * Get Magento's version.
     *
     * @return string
     */
    public function getVersion();

    /**
     * Set Magento's version.
     *
     * @param string $version
     * @return $this
     */
    public function setVersion($version);

    /**
     * Get Magento's edition.
     *
     * @return string
     */
    public function getEdition();

    /**
     * Set Magento's edition.
     *
     * @param string $edition
     * @return $this
     */
    public function setEdition($edition);

    /**
     * Get Magento's name.
     *
     * @return string
     */
    public function getName();

    /**
     * Set Magento's name.
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);
}
