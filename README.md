Madkting - Magento 2 Marketplace
=========

Turn your Magento Store in a Marketplace connecting it with Madkting.

Installation
---------

1. Add the dependency in your composer config

    ```
    composer require madkting/magento2-marketplace
    ```
    
2. Enable it in Magento

    ```
    php bin/magento module:enable Madkting_Marketplace
    ```

3. Upgrade your Magento

    ```
    php bin/magento setup:upgrade
    ```

Updates
---------

* For update this module execute the next command

    ```
    composer update madkting/magento2-marketplace
    ```
