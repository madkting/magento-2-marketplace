<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Marketplace
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento-2-marketplace
 * @copyright Copyright (c) 2018 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Marketplace\Block\System\Config\Form\Field;

use Madkting\Marketplace\Model\ManageIntegration;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Integration\Model\Integration;
use Magento\Integration\Model\IntegrationFactory;
use Magento\Integration\Model\Oauth\ConsumerFactory;
use Magento\Integration\Model\ResourceModel\Integration as IntegrationResource;
use Magento\Integration\Model\ResourceModel\Oauth\Consumer as ConsumerResource;

/**
 * Class AbstractConsumerLabel
 * @package Madkting\Marketplace\Block\System\Config\Form\Field
 */
abstract class AbstractConsumerLabel extends Field implements ConnectionLabelInterface
{
    /**
     * @var IntegrationFactory
     */
    private $integrationFactory;

    /**
     * @var IntegrationResource
     */
    private $integrationResource;

    /**
     * @var ConsumerFactory
     */
    private $consumerFactory;

    /**
     * @var ConsumerResource
     */
    private $consumerResource;

    /**
     * AbstractConsumerLabel constructor
     *
     * @param Context $context
     * @param IntegrationFactory $integrationFactory
     * @param IntegrationResource $integrationResource
     * @param ConsumerFactory $consumerFactory
     * @param ConsumerResource $consumerResource
     * @param array $data
     */
    public function __construct(
        Context $context,
        IntegrationFactory $integrationFactory,
        IntegrationResource $integrationResource,
        ConsumerFactory $consumerFactory,
        ConsumerResource $consumerResource,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );
        $this->integrationFactory = $integrationFactory;
        $this->integrationResource = $integrationResource;
        $this->consumerFactory = $consumerFactory;
        $this->consumerResource = $consumerResource;
    }

    /**
     * Retrieve element HTML markup
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $value = '';

        /**
         * Get consumer key
         *
         * @var Integration $integrationModel
         */
        $integrationModel = $this->integrationFactory->create();
        $this->integrationResource->load($integrationModel, ManageIntegration::INTEGRATION_NAME, Integration::NAME);
        $consumerId = $integrationModel->getConsumerId();
        if (!empty($consumerId)) {
            /** @var \Magento\Integration\Model\Oauth\Consumer $consumerModel */
            $consumerModel = $this->consumerFactory->create();
            $this->consumerResource->load($consumerModel, $consumerId);
            $value = $this->getValue($consumerModel);
        }

        $element->setValue($value);
        return $element->getElementHtml();
    }
}
